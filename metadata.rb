name 'powerline-shell'
maintainer 'Julien Levasseur'
maintainer_email 'contact@julienlevasseur.ca'
license 'MIT'
description 'Installs/Configures powerline-shell'
long_description 'Installs/Configures powerline-shell'
version '0.1.0'
chef_version '>= 12.14' if respond_to?(:chef_version)

issues_url 'https://github.com/julienlevasseur/powerline-shell/issues'
source_url 'https://gitlab.com/julienlevasseur/powerline-shell'

depends 'build-essential'
depends 'poise-python'

supports 'debian'
supports 'ubuntu'
