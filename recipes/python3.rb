#
# Cookbook:: chef-powerline-shell
# Recipe:: python3
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

execute 'install_powerline-shell_deps' do
  cwd '/opt/powerline-shell'
  command 'pip3 install -r requirements-dev.txt'
  user 'root'
  not_if { ::File.exist?('/opt/powerline-shell/do_not_try_to_install') }
end
