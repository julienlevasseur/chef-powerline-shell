#
# Cookbook:: powerline-shell
# Recipe:: default
#

if node['platform'] == 'ubuntu' || node['platform'] == 'debian'
  package 'fonts-powerline'
else
  package 'git'

  bash 'fonts-powerline' do
    user 'root'
    cwd '/tmp'
    code <<-EOH
    git clone https://github.com/powerline/fonts.git --depth=1
    cd fonts
    ./install.sh
    cd ..
    rm -rf fonts
    EOH
  end
end

#python_package 'powerline-shell'

directory '/opt/powerline-shell'

git '/opt/powerline-shell' do
  repository 'https://github.com/julienlevasseur/powerline-shell.git'
  revision 'master'
  action :sync
end

include_recipe 'powerline-shell::python3' if :: File.exist?('/usr/bin/python3')

execute 'install_powerline-shell' do
  cwd '/opt/powerline-shell'
  command './setup.py install'
  user 'root'
  not_if { ::File.exist?('/opt/powerline-shell/do_not_try_to_install') }
end

file '/opt/powerline-shell/do_not_try_to_install'
