#
# Cookbook:: chef-powerline-shell
# Recipe:: python2
#
# Copyright:: 2018, Julien Levasseur, All Rights Reserved.

#python_runtime '2'

execute 'install_powerline-shell_deps' do
  cwd '/opt/powerline-shell'
  command 'pip2 install -r requirements-dev.txt'
  user 'root'
  not_if { ::File.exist?('/opt/powerline-shell/do_not_try_to_install') }
end
