# # encoding: utf-8

# Inspec test for recipe powerline-shell::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

describe package('fonts-powerline') do
  it { should be_installed }
end

describe file('/usr/local/bin/powerline-shell') do
  its('type') { should eq :file }
  its('mode') { should cmp '0755' }
end

describe directory('/opt/powerline-shell') do
  it { should exist }
end

describe file('/opt/powerline-shell/do_not_try_to_install') do
  its('type') { should eq :file }
end
